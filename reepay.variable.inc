<?php

/**
 * @file
 * Variable module support for the reepay module.
 */

/**
 * @addtogroup variables
 * @{
 */

/**
 * Implements hook_variable_info().
 */
function reepay_variable_info($options) {
  $variables['reepay_public_api_key'] = array(
    'type' => 'string',
    'title' => t('Reepay Public API Key', array(), $options),
    'default' => '',
    'description' => t('Public API key for Reepay.com', array(), $options),
    'group' => 'reepay',
  );

  $variables['reepay_private_api_key'] = array(
    'type' => 'string',
    'title' => t('Reepay Private API Key', array(), $options),
    'default' => '',
    'description' => t('Private API key for Reepay.com', array(), $options),
    'group' => 'reepay',
  );

  $variables['reepay_api_url'] = array(
    'type' => 'string',
    'title' => t('Reepay API URL', array(), $options),
    'default' => 'https://api.reepay.com',
    'description' => t('URL for Reepay.com API', array(), $options),
    'group' => 'reepay',
  );

  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function reepay_variable_group_info() {
  $groups['reepay'] = array(
    'title' => t('Reepay'),
    'description' => t('Reepay.com configuration.'),
  );

  return $groups;
}

/**
 * @} End of "addtogroup variables".
 */
