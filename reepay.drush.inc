<?php

/**
 * @file
 * Drush command for downloading the Reepay PHP library.
 */

/**
 * Implements hook_drush_command().
 */
function reepay_drush_command() {
  $items = array();

  $items['reepay-download-libraries'] = array(
    'description' => 'Download PHP library for Reepay Integration.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Download the libraries.
 */
function drush_reepay_download_libraries() {
  // We will rely on `drush make` to do the download.
  $makefile = drupal_get_path('module', 'reepay') . '/reepay.make';

  $info = make_parse_info_file($makefile);
  $libraries = implode(',', array_keys($info['libraries']));

  drush_set_option('no-core', TRUE);
  drush_set_option('libraries', $libraries);
  drush_invoke('make', array($makefile, DRUPAL_ROOT));
}
