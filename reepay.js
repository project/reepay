/**
 * @file
 * Reepay JS.
 */

(function ($) {
  Drupal.behaviors.reepay = {
    attach: function (context, settings) {
      reepay.configure(Drupal.settings.reepay.publicApiKey);

      $('input[data-reepay=token]').closest('form').on('submit', (function (event) {
        var form = this;

        event.preventDefault();

        reepay.token(form, function (err, token) {
          if (err) {
            $('input[data-reepay][data-reepay!=token]', form).each(function () {
              $('label[for=' + $(this).attr('id') + ']').addClass('error');
            });

            $('.reepay-error', form).text(Drupal.t(err.message));
          }
          else {
            $('input[data-reepay][data-reepay!=token]', form).each(function () {
              $('label[for=' + $(this).attr('id') + ']').removeClass('error');
            });

            $('.reepay-error', form).text('');

            form.submit();
          }
        });
      }));
    }
  }
})(jQuery);
